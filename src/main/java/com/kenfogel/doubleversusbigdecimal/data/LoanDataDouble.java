package com.kenfogel.doubleversusbigdecimal.data;

/**
 * The data bean that uses doubles
 *
 * @author Ken Fogel
 */
public class LoanDataDouble {

    private double amountBorrowed;
    private double annualRate;
    private double term;
    private double monthlyPayment;

    public LoanDataDouble(double amountBorrowed,
            double annualRate,
            double term) {

        this.amountBorrowed = amountBorrowed;
        this.annualRate = annualRate;
        this.term = term;
        this.monthlyPayment = 0.0;
    }

    public LoanDataDouble() {
        this(0.0, 0.0, 0.0);
    }

    public double getAnnualRate() {
        return annualRate;
    }

    public void setAnnualRate(double annualRate) {
        this.annualRate = annualRate;
    }

    public double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(double monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public double getTerm() {
        return term;
    }

    public void setTerm(double term) {
        this.term = term;
    }

    public double getAmountBorrowed() {
        return amountBorrowed;
    }

    public void setAmountBorrowed(double amountBorrowed) {
        this.amountBorrowed = amountBorrowed;
    }

    @Override
    public String toString() {
        return "FinancialData{" + "amountBorrowed=" + amountBorrowed + ", annualRate=" + annualRate + ", term=" + term + ", monthlyPayment=" + monthlyPayment + '}';
    }
}
