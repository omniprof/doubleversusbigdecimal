package com.kenfogel.doubleversusbigdecimal;

import com.kenfogel.doubleversusbigdecimal.data.LoanDataBigDecimal;
import com.kenfogel.doubleversusbigdecimal.data.LoanDataDouble;
import com.patotski.performance.utils.BenchmarkUtils;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * JMH test of performance comparing Double to BigDecimal
 *
 * @author Ken Fogel
 */
@State(Scope.Benchmark)
public class DoubleVersusBigDecimal {

    private final LoanDataDouble data1;
    private final LoanDataBigDecimal data2;

    /**
     * Instantiate a data class with doubles and one with BigDecimals
     */
    public DoubleVersusBigDecimal() {
        data1 = new LoanDataDouble(5000.0, 0.05, 60);
        data2 = new LoanDataBigDecimal(new BigDecimal("5000.0"), new BigDecimal("0.05"), new BigDecimal("60"));
    }

    /**
     * Calculate a loan payment using doubles
     */
    @Benchmark
    public void loanCalculationDouble() {
        var monthlyRate = data1.getAnnualRate() / 12.0;
        var temp = 1 + monthlyRate;
        temp = Math.pow(temp, data1.getTerm());
        temp = 1.0 / temp;
        temp = 1 - temp;
        temp = monthlyRate / temp;
        temp = data1.getAmountBorrowed() * temp;
        double value = Double.parseDouble(String.format("%.2f", temp));
        data1.setMonthlyPayment(value);
    }

    /**
     * Calculate a loan payment using BigDecimal
     */
    @Benchmark
    public void loanCalculationBigDecimal() throws ArithmeticException {

        var monthlyRate = data2.getAnnualRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);
        // (1+rate)
        var temp = BigDecimal.ONE.add(monthlyRate);
        // (1+rate)^term
        temp = temp.pow(data2.getTerm().intValueExact());
        // BigDecimal pow does not support negative exponents so divide 1 by the result
        temp = BigDecimal.ONE.divide(temp, MathContext.DECIMAL64);
        // 1 - (1+rate)^-term
        temp = BigDecimal.ONE.subtract(temp);
        // rate / (1 - (1+rate)^-term)
        temp = monthlyRate.divide(temp, MathContext.DECIMAL64);
        // pv * (rate / 1 - (1+rate)^-term)
        temp = data2.getAmountBorrowed().multiply(temp);
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        data2.setMonthlyPayment(temp.abs());
    }

    /**
     * Just do the calculation
     */
    public void perform() {
        loanCalculationDouble();
        System.out.printf("Double: %.2f%n", data1.getMonthlyPayment());
        loanCalculationBigDecimal();
        System.out.printf("BigDecimal: %s%n", data2.getMonthlyPayment());
    }

    /**
     * Where it all begins
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        new DoubleVersusBigDecimal().perform();
        BenchmarkUtils.runBenchmark(DoubleVersusBigDecimal.class);
    }
}
